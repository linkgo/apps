Release Notes
=============

BSC Application v0.5.1
----------------------

Date: Nov 17, 2015
Brief: Support Israel server.
       And enable JSON format message to improve efficiency.

Enabled Features:
    * Support JSON format for both up & down message.


BSC Application v0.5
--------------------

Date: Nov 10, 2015
Brief: The first release commited into Rlease Notes.

Enabled Features:
    * UID for registration in the Cloud.
    * MQTT over Ethernet
    * MQTT over WiFi(ESP8266)
    * Network Manager and Arbitration
    * Relays Control
    * PWM Output
    * Modbus Server (for read only access)
    * Analog Input
    * Realtime Data Publishing
    * Realtime Device Control
